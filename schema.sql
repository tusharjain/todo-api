CREATE TABLE `users` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(255),
  `password` varchar(255),
  `active_tasks` int
);

CREATE TABLE `tasks` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `description` text,
  `user_id` int, 
  `status` varchar(10)
);

CREATE TABLE `subtasks` (
  `id` int PRIMARY KEY,
  `description` text,
  `task_id` int, 
  `status` varchar(10)
);

ALTER TABLE `tasks` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `subtasks` ADD FOREIGN KEY (`id`) REFERENCES `tasks` (`id`);
