```
Routes:  
Add a user: POST localhost:3000/users   
            body : {  
                username: "Tushar",  
                password: 123456  
            }  
view all users: GET localhost:3000/users
search a user: GET localhost:3000/users/1
               where 1 is user id
edit a user: PUT localhost:3000/users/1
             body: {
                 username: "Tushar J",
                 password: 123415,
                 active_tasks: 5
             }
delete a user: DELETE localhost:3000/users/1


Add a task: POST localhost:3000/tasks 
            body: {
                description: "task one",
                user_id: 1,
                status: "active"
            }
View all tasks for a user: GET localhost:3000/users/<user id>/tasks
Search a task: GET localhost:3000/users/<user id>/tasks/<task id>
Edit a task: PUT localhost:3000/users/<user id>/tasks/<task id>
             body: {
                description: "task one",
                user_id: <user id>,
                status: "active"
             }
             Authentication: Bearer <token>
Delete a task: DELETE localhost:3000/users/<user id>/tasks/<task id>
               Authentication: Bearer <token>


For bearer token: POST localhost:3000/login
                  body : {
                     username: "Tushar",
                     password: 123456
                  }
```
