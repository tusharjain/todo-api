const Auth = require("../models/auth.model");
const config = require("../config/auth.config");
var jwt = require("jsonwebtoken");
const userRoutes = require("../routes/user.routes");

exports.signin = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    const { username, password } = req.body; 

    Auth.findUser(username, password, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Invalid credentials.`,
              accessToken: null
            });
          } else {
            res.status(500).send({
              message: "Error retrieving User"
            });
          }
        } 
        
        else {
            let token = jwt.sign({ user: username }, config.secret);
            res.status(200).send({
                accessToken: token
            });
        } 
    });
}