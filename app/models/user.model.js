const sql = require("./db.js");

// constructor
const User = function(user) {
    this.username = user.username;
    this.password = user.password; 
    this.active_tasks = user.active_tasks; 
};

User.create = (newUser, result) => {
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if(err) {
            console.log("error: ", err);
            result(err, null);
            return; 
        }

        console.log("created user: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
    });
};

User.getAll = result => {
  sql.query("SELECT * FROM users", (err, res) => {
      if (err) {
          console.log("error: ", err); 
          result(null, err); 
          return; 
      }

      console.log("users: ", res); 
      result(null, res); 
  });
};

User.findById = (id, result) => {
    sql.query(`SELECT * FROM users WHERE id = ${id}`, (err, res) => {
        if(err) {
            console.log("error: ", err); 
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return; 
        }

        
        result({ kind: "not_found" }, null); 
    });
};


User.updateById = (id, user, result) => {
    sql.query(
      "UPDATE users SET username = ?, active_tasks = ? WHERE id = ?",
      [user.username, user.active_tasks, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated user: ", { id: id, ...user });
        result(null, { id: id, ...user });
      }
    );
  };
  
  User.remove = (id, result) => {
    sql.query("DELETE FROM users WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("deleted user with id: ", id);
      result(null, res);
    });
  };
  

  
  module.exports = User;