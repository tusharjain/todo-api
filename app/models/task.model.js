const sql = require("./db.js");

// constructor
const Task = function(task) {
    this.description = task.description;
    this.user_id = task.user_id;
    this.status = task.status; 
};

Task.create = (newTask, result) => {
    sql.query("INSERT INTO tasks SET ?", newTask, (err, res) => {
        if(err) {
            console.log("error: ", err);
            result(err, null);
            return; 
        }

        console.log("created task: ", { id: res.insertId, ...newTask });
        result(null, { id: res.insertId, ...newTask });
    });
};
  
Task.getAll = (userId, result) => {
    sql.query(`SELECT * FROM tasks WHERE user_id = ${userId}`, (err, res) => {
        if (err) {
            console.log("error: ", err); 
            result(null, err); 
            return; 
        }

        result(null, res); 
    });
};

Task.findById = (userId, taskId, result) => {
    sql.query(`SELECT * FROM tasks WHERE id = ${taskId} AND user_id = ${userId}` , (err, res) => {
        if(err) {
            console.log("error: ", err); 
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found task: ", res[0]);
            result(null, res[0]);
            return; 
        }

        
        result({ kind: "not_found" }, null); 
    });
};


Task.updateById = (userId, taskId, task, result) => {
    sql.query(
      `UPDATE tasks SET description = ?, user_id = ?, status = ? WHERE id = ? AND user_id = ?`,
      [task.description, task.user_id, task.status, taskId, userId],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated task: ", { id: taskId, ...task });
        result(null, { id: taskId, ...task });
      }
    );
};
  
Task.remove = (userId, taskId, result) => {
  sql.query(`DELETE FROM tasks WHERE id = ? AND user_id = ?`, taskId, userId, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted task with id: ", taskId);
    result(null, res);
  });
};

  
module.exports = Task;