const tasks = require("../controllers/task.controller.js");
const { verifyToken } = require("../middleware/authJwt");

module.exports = app => {

    
    app.post("/tasks", tasks.create); // 

    
    app.get("/users/:userId/tasks", tasks.findAll); // 

    
    app.get("/users/:userId/tasks/:taskId", tasks.findOne);

    
    app.put("/users/:userId/tasks/:taskId", verifyToken, tasks.update);

    
    app.delete("/users/:userId/tasks/:taskId", verifyToken, tasks.delete);
};